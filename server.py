from flask import Flask, jsonify, abort, make_response, request
from tasks import tasks 
import time
app = Flask(__name__)


@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not found'}), 404)


#получение всех тасков
@app.route("/api/task/", methods=['GET'])
def get_task():
	return jsonify({'tasks':tasks})

#получение тасков по id
@app.route("/api/task/<int:task_id>", methods=['GET'])
def get_task_by_id(task_id):
	task_by_id = search_by_id(task_id)
	if not task_by_id:
		return abort(404)
	return jsonify({'task':task_by_id})

def search_by_id(task_id):
	flag = False
	task_by_id={}
	for task_item in tasks:
		for key in task_item:
			if key == 'id' and task_item[key] == task_id:
				task_by_id = task_item
				flag = True
	if flag:
		return task_by_id
	else:
		return False


@app.route("/api/task/",  methods=['POST'])
def create_task():
	if not request.json:
		abort(400)
	try:
		task = {
			'id': time.time(),
			'title':request.json['title'],
			'desc':request.json['desc'],
			'finished':request.json['finished']
		}
	except:
		return jsonify({'error': 'not a complete set of data'})
	
	answer = checkError(task)
	try:
		if answer['error']:
			return jsonify(answer)
	except:
		tasks.append(answer)	
		return jsonify(answer), 200

@app.route("/api/task/<int:task_id>", methods=['PUT'])
def update_task(task_id):
	task_by_id = search_by_id(task_id)
	task_by_id['title'] = request.json.get('title', task_by_id['title'])
	task_by_id['desc'] = request.json.get('desc', task_by_id['desc'])
	task_by_id['finished'] = request.json.get('finished', task_by_id['finished'])
	return jsonify(task_by_id)

@app.route("/api/task/<int:task_id>", methods=['DELETE'])
def delete_task(task_id):
	task_by_id = search_by_id(task_id)
	if not task_by_id:
		abort(404)
	tasks.remove(task_by_id)
	return '200'

def checkError(task):
	titleError = {}
	if type(task['title']).__name__ != 'str':
		titleError = {'error': 'title is not string'}
		return titleError
	elif len(task['title']) == 0:
		titleError = {'error': 'title is empty'}
		return titleError
	
	
	elif type(task['finished']).__name__ != 'bool':
		errors = {'error':'finished is not boolean'}
		return errors
	else:
		return task

if __name__ == '__main__':
	app.run()